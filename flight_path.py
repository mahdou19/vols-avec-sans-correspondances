from typing import List
from flight import Flight
from airport import Airport

class FlightPath:
    def __init__(self, src_airport: Airport) -> None:
        self.path = [src_airport]
        self.flights_list = []

    def add(self, dst_airport: Airport, via_flight: Flight) -> None:
        if via_flight.src_code != self.path[-1].code:
            raise FlightPathBroken("Le vol ajouté ne passe pas par le dernier aéroport du chemin.")
        self.path.append(dst_airport)
        self.flights_list.append(via_flight)

    def flights(self) -> List[Flight]:
        return self.flights_list

    def airports(self) -> List[Airport]:
        return self.path

    def steps(self) -> int:
        return len(self.flights_list)

    def duration(self) -> float:
        return sum(flight.duration for flight in self.flights_list)

    def __str__(self):
        return f'{self.path} ({self.flights_list})'

 

class FlightPathBroken(Exception):
    pass

class FlightPathDuplicate(Exception):
    pass