from flight_map import FlightMap
from airport import Airport
from flight import Flight



# airport = Airport("Paris Charles de Gaulle Airport", "CDG", 49.012779, 2.55)
# flight = Flight("CDG", "JFK", 7.5)
# print(flight)

fm = FlightMap()
fm.import_airports("aeroports.csv")
fm.import_flights("flights.csv")
airports = fm.airports()
flights = fm.flights()
find = fm.airport_find('NAN')
flight_exist = fm.flight_exist('CDG', 'AMS')
# if flight_exist:
#     print("Un vol direct existe ")
# else:
#     print("Il n'y a pas de vol direct")
# flight_where = fm.flights_where('NAN')
# for flight in flight_where:
#     print(flight)
# airports_from = fm.airports_from('CDG')
# for airport in airports_from:
#     print(airport.name) 
