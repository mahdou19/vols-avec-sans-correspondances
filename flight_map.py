import csv
import re
from typing import List
from flight import Flight
from airport import Airport

class FlightMap:
    def __init__(self):
        self.airports_dict = {}
        self.flights_list = []

    def import_airports(self, csv_file: str) -> None:
        with open(csv_file, 'r') as f:
            data = csv.reader(f)

            for row in data:
                name, code, lat, long = row
                code = re.sub(r'^\s*"|"\s*$', '', code)
                lat_float = float(re.search(r'\d+(\.\d+)?', lat).group())
                long_float = float(re.search(r'\d+(\.\d+)?', long).group())
                airport = Airport(name, code, lat_float, long_float)
                self.airports_dict[code] = airport

    def import_flights(self, csv_file: str) -> None:
        with open(csv_file, 'r') as f:
            data = csv.reader(f)

            for row in data:
                src_code, dst_code, duration = row
                dst_code = re.sub(r'^\s*"|"\s*$', '', dst_code)
                flight = Flight(src_code, dst_code, float(duration))
                self.flights_list.append(flight)

                if dst_code in self.airports_dict:
                    inverse_flight = Flight(dst_code, src_code, float(duration))
                    self.flights_list.append(inverse_flight)

    def airports(self) -> List[Airport]:
        return self.airports_dict.values()
    
    def flights(self) -> List[Flight]:
        return self.flights_list

    def airport_find(self, airport_code: str) -> Airport:
        return self.airports_dict.get(airport_code)

    def flight_exist(self, src_airport_code: str, dst_airport_code: str) -> bool:
        for flight in self.flights_list:
            if flight.src_code == src_airport_code and flight.dst_code == dst_airport_code:
                return True
        return False

    def flights_where(self, airport_code: str) -> List[Flight]:
        flights_where = []
        for flight in self.flights_list:
            if flight.src_code == airport_code or flight.dst_code == airport_code:
                flights_where.append(flight)
        return flights_where

    def airports_from(self, airport_code: str) -> List[Airport]:
        airports_from = []
        for flight in self.flights_list:
            if flight.src_code == airport_code:
                airports_from.append(self.airports_dict[flight.dst_code])
        return airports_from  

    def __str__(self):
        return f'{self.flights_list}'


# fm = FlightMap()
# fm.import_airports("aeroports.csv")
# fm.import_flights("flights.csv")
# find = fm.airport_find('NAN')
# if find is not None:
#     print(f'{find.name}')
# else:
#     print('Aucun aéroport trouvé')

# if fm.flight_exist('CDG', 'AMS'):
#     print("Il existe un vol direct .")
# else:
#     print("Il n'existe pas de vol direct ")
# # fm.flights()

# airports = fm.airports_from("AMS")
# print(airports)

# flights = fm.flights_where('ICN')
# for flight in flights:
#     print(f"Le vol {flight.src_code}-{flight.dst_code} dure {flight.duration} heures.")